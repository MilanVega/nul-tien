﻿using AutoMapper;
using Common;
using Common.DTOs;
using DataLayer.Models;
using System;
using System.Linq;

namespace BusinessLayer
{
	public class ArticleService : GenericService<Article>
	{
		private SupplierService _supplierService = new SupplierService();
		protected IMapper _iMapper;

		public ArticleService()
		{
			var config = new MapperConfiguration(cfg => {
				cfg.CreateMap<Article, ArticleDTO>().ReverseMap();
			});

			_iMapper = config.CreateMapper();
		}

		public ArticleDTO Order(int articleId, int maximumPrice)
		{
			BaseSupplier chosenSupplier = 
				_supplierService.GetSuppliers()
				.FirstOrDefault(supplier =>
					supplier.IsArticleInInventory(articleId) &&
					supplier.GetArticle(articleId)
						.ArticlePrice <= maximumPrice);

			if (chosenSupplier == null)
			{
				Output.Print("Could not find any article with required parameters from suppliers.");
				return null;
			}
			else
			{
				return _iMapper.Map<ArticleDTO>(chosenSupplier.GetArticle(articleId));
			}
		}

		public void SaveArticle(ArticleDTO articleDTO, int buyerId)
		{
			Logger.Debug("Trying to save article with id = " + articleDTO.Id);

			try
			{
				Article article = _iMapper.Map<Article>(articleDTO);
				article.IsSold = true;
				article.SoldDate = DateTime.Now;
				article.BuyerUserId = buyerId;

				Save(article);

				Logger.Info("Article with id = " + article.Id + " has been sold.");
			}
			catch (ArgumentNullException ex)
			{
				Logger.Error("Could not save article with id = " + articleDTO.Id + " with error message: " + ex.Message);
				Output.Print("Article not saved!");
			}
			catch (Exception ex)
			{
				Logger.Error("Error while trying to sell article with id = " + articleDTO.Id + " with error message: " + ex.Message);
				Output.Print("Article not saved!");
			}
		}

		public ArticleDTO GetArticleById(int articleId)
		{
			return _iMapper.Map<ArticleDTO>(GetById(articleId));
		}
	}
}
