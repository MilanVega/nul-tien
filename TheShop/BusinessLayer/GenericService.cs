﻿using AutoMapper;
using Common;
using Common.DTOs;
using DataLayer;
using DataLayer.Models;

namespace BusinessLayer
{
    public class GenericService<T> where T : BaseModel
    {
        protected GenericDatabaseDriver<T> _genericDatabaseDriver = GenericDatabaseDriver<T>.GetInstance();

        public GenericService()
        {
        }

		public bool DoesRecordExist(int id)
		{
			return _genericDatabaseDriver.DoesRecordExist(id);
		}

		public T GetById(int id)
		{
			return _genericDatabaseDriver.GetById(id);
		}

		public void Save(T record)
		{
			_genericDatabaseDriver.Save(record);
		}
	}
}
