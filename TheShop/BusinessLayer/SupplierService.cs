﻿using DataLayer.Models;
using System.Collections.Generic;

namespace BusinessLayer
{
    public class SupplierService
    {
        private List<BaseSupplier> _suppliers = new List<BaseSupplier>();

        public SupplierService()
        {
            _suppliers.Add(new Supplier1());
            _suppliers.Add(new Supplier2());
            _suppliers.Add(new Supplier3());
        }

        public List<BaseSupplier> GetSuppliers()
        {
            return _suppliers;
        }
    }
}
