﻿using System;
using System.Text;

namespace Common.DTOs
{
	public class ArticleDTO
	{
		public int Id { get; set; }
		public string NameOfArticle { get; set; }
		public int ArticlePrice { get; set; }
		public bool IsSold { get; set; }
		public DateTime SoldDate { get; set; }
		public int BuyerUserId { get; set; }

		public override string ToString()
		{
			StringBuilder articleDescription = new StringBuilder();

			articleDescription
				.AppendLine("-------------------------------")
				.Append("Name: ").AppendLine(NameOfArticle)
				.Append("Price: ").AppendLine(ArticlePrice.ToString())
				.Append("Is sold: ").AppendLine(IsSold.ToString())
				.AppendLine("-------------------------------");

			return articleDescription.ToString();
		}
	}
}
