﻿using System;

namespace Common
{
    public static class Output
    {
        public static void Print(string message)
        {
            Console.WriteLine(message);
        }
    }
}
