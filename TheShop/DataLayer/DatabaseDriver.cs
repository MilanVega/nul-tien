﻿using DataLayer.Models;
using System.Collections.Generic;
using System.Linq;

namespace DataLayer
{
	// In memory implementation
	public class DatabaseDriver
	{
		private static DatabaseDriver _databaseDriverInstance = null;
		private List<Article> _articles = new List<Article>();

		private DatabaseDriver()
		{
		}

		public static DatabaseDriver GetInstance()
		{
			if (_databaseDriverInstance == null)
			{
				_databaseDriverInstance = new DatabaseDriver();
			}

			return _databaseDriverInstance;
		}

		public bool DoesArticleExist(int id)
		{
			return _articles.Any(article => article.Id == id);
		}

		public Article GetById(int id)
		{
			return _articles.Single(x => x.Id == id);
		}

		public void Save(Article article)
		{
			_articles.Add(article);
		}
	}
}
