﻿using DataLayer.Models;
using System.Collections.Generic;
using System.Linq;

namespace DataLayer
{
	// In memory implementation
	public class GenericDatabaseDriver<T> where T : BaseModel
	{
		private static GenericDatabaseDriver<T> _databaseDriverInstance = null;
		private List<T> database = new List<T>();

		private GenericDatabaseDriver()
		{
		}

		public static GenericDatabaseDriver<T> GetInstance()
		{
			if (_databaseDriverInstance == null)
			{
				_databaseDriverInstance = new GenericDatabaseDriver<T>();
			}

			return _databaseDriverInstance;
		}

		public bool DoesRecordExist(int id)
		{
			return database.Any(record => record.Id == id);
		}

		public T GetById(int id)
		{
			return database.Single(record => record.Id == id);
		}

		public void Save(T record)
		{
			database.Add(record);
		}
	}
}
