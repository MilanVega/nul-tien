﻿using System;

namespace DataLayer.Models
{
	public class Article : BaseModel
	{
		public string NameOfArticle { get; set; }
		public int ArticlePrice { get; set; }
		public bool IsSold { get; set; }
		public DateTime SoldDate { get; set; }
		public int BuyerUserId { get; set; }
	}
}
