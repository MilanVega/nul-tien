﻿using System.Collections.Generic;
using System.Linq;

namespace DataLayer.Models
{
	public abstract class BaseSupplier
	{
		protected List<Article> _articles = new List<Article>();

		public BaseSupplier()
		{
		}

		public bool IsArticleInInventory(int id)
		{
			return _articles.Any(article => article.Id == id);
		}

		public Article GetArticle(int id)
		{
			return _articles.First(article => article.Id == id);
		}
	}
}
