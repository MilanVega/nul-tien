﻿namespace DataLayer.Models
{
    public class Supplier1 : BaseSupplier
    {
        public Supplier1()
        {
            _articles.Add(
                new Article(){
                    Id = 1,
                    NameOfArticle = "Article from supplier1",
                    ArticlePrice = 458
                });
        }
    }
}
