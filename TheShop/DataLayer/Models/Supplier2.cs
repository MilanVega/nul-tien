﻿namespace DataLayer.Models
{
    public class Supplier2 : BaseSupplier
    {
        public Supplier2()
        {
            _articles.Add(
                new Article()
                {
                    Id = 1,
                    NameOfArticle = "Article from supplier2",
                    ArticlePrice = 459
                });
        }
    }
}
