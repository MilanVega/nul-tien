﻿namespace DataLayer.Models
{
    public class Supplier3 : BaseSupplier
    {
        public Supplier3()
        {
            _articles.Add(
                new Article()
                {
                    Id = 1,
                    NameOfArticle = "Article from supplier3",
                    ArticlePrice = 460
                });
        }
    }
}
