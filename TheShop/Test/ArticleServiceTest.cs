﻿using BusinessLayer;
using Common.DTOs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test
{
    [TestClass]
    public class ArticleServiceTest
    {
        private ArticleService _articleService;

        public ArticleServiceTest()
        {
            _articleService = new ArticleService();
        }

        [TestMethod]
        public void TestOrder()
        {

            var article = _articleService.Order(1, 500);
            Assert.AreNotEqual(article, null);

            article = _articleService.Order(1, 300);
            Assert.AreEqual(article, null);
        }

        [TestMethod]
        public void TestSave()
        {
            _articleService.SaveArticle(
                new ArticleDTO()
                {
                    Id = 1000,
                    ArticlePrice = 1000,
                    NameOfArticle = "Test article"
                },
                10);

            var article = _articleService.GetArticleById(1000);

            Assert.AreNotEqual(article, null);
            Assert.AreEqual(article.Id, 1000);
        }
    }
}
