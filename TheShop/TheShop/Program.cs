﻿using BusinessLayer;
using Common;
using Common.DTOs;
using System;

namespace TheShop
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			var articleService = new ArticleService();

			try
			{
				int articleId = 1;
				int maximumPrice = 458;
				int buyerId = 10;

				//order
				ArticleDTO foundArticle = articleService.Order(articleId, maximumPrice);

				if (foundArticle != null)
				{
					Output.Print("Ordered article with name: " + foundArticle.NameOfArticle);

					articleService.SaveArticle(foundArticle, buyerId);
				}
			}
			catch (Exception ex)
			{
				Logger.Error(ex.Message);
				Console.WriteLine("Error occurred!");
			}

			try
			{
				//print article on console
				var article = articleService.GetArticleById(1);

				if (article != null)
				{
					Console.WriteLine("Info of the processed article: ");
					Console.WriteLine(article.ToString());
				}
				else
				{
					Output.Print("Article not found!");
				}
			}
			catch (Exception ex)
			{
				Logger.Error(ex.Message);
				Output.Print("Article not found!");
			}

			Console.ReadKey();
		}
	}
}